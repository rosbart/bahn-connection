import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import './transport-connection-list';
import { connections, generate } from '@rosbart/bahn-connection-retriever/src/url-generator'
import { retrieve } from '@rosbart/bahn-connection-retriever/src/web-document-retriever'
import { toConnections } from '@rosbart/bahn-connection-retriever/src/connection-converter'

class BahnConnections extends PolymerElement {
    static get template() {
      return html`
        <transport-connection-list connections="[[connections]]"></transport-connection-list>
      `;
    }
    static get properties() {
      return {
        connections: {
          type: Array,
          value: []
        },
        from: {
          type: String,
          value: 'Kiefersfelden'
        },
        to: {
          type: String,
          value: 'Ingolstadt Hbf'
        },
        at: {
          type: Date,
          value: Date.UTC()
        }
      };
    }

    async ready() {
      super.ready();
      const url = generate(connections()
                          .from(this.from)
                          .to(this.to)
                          .at(this.at));

      const document = await retrieve(url);
      this.connections = toConnections(document, this.at);
    }
  }

  window.customElements.define('bahn-connections', BahnConnections);
