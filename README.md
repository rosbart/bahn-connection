# \<bahn-connections\> webcomponent

<div>
    <img style="display: block; margin: auto" src="./assets/example.png" width="500px">
</div>

## How to use

### prerequisites

1. install npm
2. ```npm install https://gitlab.com/rosbart/bahn-connection.git```

### in a single page application

```js
import '@rosbart/bahn-connections/bahn-connections';
```

```html
<bahn-connections from="Kiefersfelden" to="Ingolstadt Hbf"></bahn-connections>
```

_Note: the current time is set to now utc_

## Run demo

### Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) and npm (packaged with [Node.js](https://nodejs.org)) installed. Run `npm install` to install your element's dependencies, then run `polymer serve` to serve your element locally.

### Viewing Your Element

```
$ polymer serve
```

## Maintainer

* [Phibart](https://gitlab.com/phibart)
* [Josch Rossa](https://gitlab.com/josros)