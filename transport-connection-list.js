import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-spinner/paper-spinner';

import './transport-connection-item';

class TransportConnectionList extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        .spinner-wrapper {
          text-align: center;
        }
        transport-connection{
          --date-color: black;
        }
      </style>

      <template is="dom-if" if="[[!loading]]">
        <template is="dom-repeat" items="[[connections]]" class="connections">
          <transport-connection-item
            departure-date="[[item.departureDate]]"

            departure-time="[[item.departureTime]]"
            departure-location="[[item.departureLocation]]"
            estimated-departure-time-color="[[_resolveDelayColor(item.departureTime, item.estimatedDepartureTime)]]"
            estimated-departure-time="[[item.estimatedDepartureTime]]"

            arrival-time="[[item.arrivalTime]]"
            arrival-location="[[item.arrivalLocation]]"
            estimated-arrival-time-color="[[_resolveDelayColor(item.arrivalTime, item.estimatedArrivalTime)]]"
            estimated-arrival-time="[[item.estimatedArrivalTime]]"

            background-color="[[_resolveBackgroundColor(index)]]"
            >
          </transport-connection-item>
        </template>
      </template>

      <template is="dom-if" if="[[loading]]">
        <div class="spinner-wrapper">
          <paper-spinner active></paper-spinner>
        </div>
      </template>
    `;
  }

  static get properties() {
    return {
      connections: {
        type: Array,
        value: [],
        observer: '_updateLoadingState'
      },
      loading: {
        type: Boolean,
        value: true
      }
    };
  }

  _updateLoadingState(){
    this.loading = !this.connections || this.connections.length === 0;
  }

  _resolveDelayColor(should, is){
    return should == is ? 'green' : 'red';
  }

  _resolveBackgroundColor(index){
    return index % 2 === 0 ?  'transparent' : '#f5f5f5';
  }
}

window.customElements.define('transport-connection-list', TransportConnectionList);
