import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-card/paper-card';
import '@polymer/iron-flex-layout/iron-flex-layout-classes';

class TransportConnectionItem extends PolymerElement {
  static get template() {
    return html`
      <style is="custom-style" include="iron-flex iron-flex-alignment">
        :host {
          --transport-connection-background: transparent;
          --estimated-arrival-time-color: #000000;
          --estimated-departure-time-color: #000000;
          --date-color: lightgray;
        }

        .container {
          @apply --layout-horizontal;
          @apply --layout-wrap;
          background-color: var(--transport-connection-background);
        }

        .estimated-arrival-time {
          color: var(--estimated-arrival-time-color);
        }

        .estimated-departure-time {
          color: var(--estimated-departure-time-color);
        }

        .connection {
          @apply --flex-6;
          flex: 1;
          padding: 6px;
          padding-bottom: 0px;
          margin: auto;
        }

        .connection-line {
          height: 4px;
          background: #000000;
        }

        .connection-line::before,
        .connection-line::after {
          content: '';
          width: 16px;
          height: 16px;
          margin-top: -6px;
          border-radius: 50%;
          background-color: #000000;
        }

        .connection-line::before {
          float: left;
          margin-left: -1px;
        }

        .connection-line::after {
          float: right;
          margin-right: -1px;
        }

        .departure, .arrival {
          @apply --flex-3;
          flex: 1;
          text-align: center;
        }

        .time {
          font-weight: bold;
        }

        .date {
          font-size: 8px;
          color: var(--date-color);
        }

        .connection-element {
          padding-top: 15px;
          padding-bottom: 15px;
        }

        @media screen and (max-width: 400px) {
          .md {
            display: none;
          }
        }
      </style>


    <paper-card class="container">

      <div class="departure connection-element">
        <div class="location">
          [[departureLocation]]
        </div>
        <div class="time">
          [[departureTime]]
        </div>
        <template is="dom-if" if="[[isVisible(estimatedDepartureTime)]]">
          <div class="estimated-departure-time">
            [[estimatedDepartureTime]]
          </div>
        </template>
        <template is="dom-if" if="[[isVisible(departureDate)]]">
          <div class="date">
            [[departureDate]]
          </div>
        </template>
      </div>

      <slot name="connection">
        <div class="connection md connection-element">
          <div class="connection-line"></div>
        </div>
      </slot>

      <div class="arrival connection-element">
        <div class="location">
          [[arrivalLocation]]
        </div>
        <div class="time">
          [[arrivalTime]]
        </div>
        <template is="dom-if" if="[[isVisible(estimatedArrivalTime)]]">
          <div class="estimated-arrival-time">
            [[estimatedArrivalTime]]
          </div>
        </template>
        <template is="dom-if" if="[[isVisible(arrivalDate)]]">
          <div class="date">
            [[arrivalDate]]
          </div>
        </template>
      </div>

    </paper-card>

    `;
  }
  static get properties() {
    return {
      departureTime: {
        type: String,
      },
      departureDate: {
        type: String,
      },
      departureLocation: {
        type: String,
      },
      estimatedDepartureTime:{
        type: String,
      },
      estimatedDepartureTimeColor: {
        type: String,
        observer: '_changeDepartureTimeColor'
      },

      arrivalTime: {
        type: String,
      },
      arrivalDate: {
        type: String,
      },
      arrivalLocation: {
        type: String,
      },
      estimatedArrivalTime:{
        type: String,
      },
      estimatedArrivalTimeColor: {
        type: String,
        observer: '_changeArrivalTimeColor'
      },

      backgroundColor: {
        type: String,
        observer: '_changeBackgroundColor'
      }
    };
  }

  isVisible(value){
    return value !== undefined;
  }

  _changeArrivalTimeColor(newValue){
    this.updateStyles({
      '--estimated-arrival-time-color': newValue,
    });
  }

  _changeDepartureTimeColor(newValue){
    this.updateStyles({
      '--estimated-departure-time-color': newValue,
    });
  }

  _changeBackgroundColor(newValue){
    this.updateStyles({
      '--transport-connection-background': newValue,
    });
  }
}

window.customElements.define('transport-connection-item', TransportConnectionItem);
